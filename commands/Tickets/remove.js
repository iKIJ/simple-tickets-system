/**
 * 
 * Simple Tickets System - Made by Lyex
 * Lyex (c) 2019
 * 
 */ 

module.exports = {
    config: {
        name: "remove", // أسم الأمر الرئيسي..
        aliases: [], // بإمكانك إضافة أختصارات اخرى..
        permission: 0, // مستوى البرمشنات لأستخدام الأمر, 0 = جميع الأعضاء..
        category: "tickets"
    },
    exec: async (client, msg) => {
        const { RichEmbed } = require("discord.js");

        if(!msg.channel.name.toLowerCase().startsWith("ticket-")) {
            let embed = new RichEmbed().setColor("RED").addField(`:x: Forbidden`, `Command has been cancelled - you are not allowed to use this command outside a ticket!`);
            return msg.channel.send({embed: embed});
        }

        let member = msg.mentions.members.first();
        if(!member || member.id === client.user.id) {
            let embed = new RichEmbed().setColor("RED").addField(`:x: Invalid Arguments`, `**Usage:** \`remove @member\`\n**Example:** \`remove @${msg.author.tag}\``);
            return msg.channel.send({embed: embed});
        }
        if(msg.channel.topic === member.id) {
            let embed = new RichEmbed().setColor("RED").addField(`:x: Forbidden`, `You cannot remove the ticket author, if you want to leave just ask the staff to remove you!\n\n**Usage:** \`remove @member\`\n**Example:** \`remove @${msg.author.tag}\``);
            return msg.channel.send({embed: embed});
        }
        if(member.id == msg.author.id) { let embed = new RichEmbed().setColor("ORANGE").addField(`:warning: Warning`, `You have to use **close** command instead!`); return msg.channel.send({embed: embed}); }
        if(!msg.channel.permissionsFor(member).has(['SEND_MESSAGES', 'VIEW_CHANNEL', 'READ_MESSAGE_HISTORY'])) {
            let embed = new RichEmbed().setColor("ORANGE").addField(`:warning: Warning`, `**${member.user.tag}** is not in this ticket to remove them!`);
            return msg.channel.send({embed: embed});
        }
        msg.channel.overwritePermissions(member.id, { SEND_MESSAGES: false, VIEW_CHANNEL: false, READ_MESSAGE_HISTORY: false });
        let embed = new RichEmbed().setColor("BLUE").addField(`:white_check_mark: User Removed!`, `Successfully removed **${member.user.tag}** from the ticket!`).setFooter(`Removed by: ${msg.author.tag}`);
        msg.channel.send({embed: embed});
    }
};