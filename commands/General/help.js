/**
 * 
 * Simple Tickets System - Made by Lyex
 * Lyex (c) 2019
 * 
 */ 

module.exports = {
    config: {
        name: "help", // أسم الأمر الرئيسي..
        aliases: ['?'], // بإمكانك إضافة أختصارات اخرى..
        permission: 0, // مستوى البرمشنات لأستخدام الأمر, 0 = جميع الأعضاء..
        category: "general"
    },
    exec: async (client, msg, args, storage) => {
        const { RichEmbed } = require("discord.js");
        const functions = require("../../util/functions");
        let embed = new RichEmbed()
            .setColor("#FFFFFF")
            .setAuthor(`Help Menu`, msg.author.displayAvatarURL)
            .addField(`Tickets`, `\`${functions.getCmds(client, "Tickets")}\``)
            .addField(`Prefix`, `**Prefix(es)**: \`${storage.bot.prefix.join(", ")}\``)
            .setTimestamp(Date.now())
            .setFooter(client.user.tag);
        msg.author.send({embed: embed})
        .then(() => msg.channel.send(`📬 **Sent to your DM**`))
        .catch(err => msg.channel.send({embed: embed}));
    }
};