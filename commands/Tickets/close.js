/**
 * 
 * Simple Tickets System - Made by Lyex
 * Lyex (c) 2019
 * 
 */ 

module.exports = {
    config: {
        name: "close", // أسم الأمر الرئيسي..
        aliases: ['closeticket'], // بإمكانك إضافة أختصارات اخرى..
        permission: 0, // مستوى البرمشنات لأستخدام الأمر, 0 = جميع الأعضاء..
        category: "tickets"
    },
    exec: async (client, msg) => {
        const { RichEmbed } = require("discord.js");

        if(!msg.channel.name.toLowerCase().startsWith("ticket-")) {
            let embed = new RichEmbed().setColor("RED").addField(`:x: Forbidden`, `Command has been cancelled - you are not allowed to use this command outside a ticket!`);
            return msg.channel.send({embed: embed});
        }
        let cmsg = await msg.channel.send({embed: new RichEmbed().setColor("ORANGE").setFooter(`${client.user.tag}`, client.user.displayAvatarURL).addField(`⏲ Confirmation`, `Please react with :white_check_mark: to ensure you want to close the ticket!`)})
        cmsg.react("✅");
        
        let filter = (reaction, user) => reaction.emoji.name === "✅" && user.id === msg.author.id;
        
        let collector = cmsg.createReactionCollector(filter, { max: 1, time: 60000 });
        setTimeout(function() { 
            if(msg.guild.channels.get(msg.channel.id)) { cmsg.delete(); msg.delete(); } 
        }, 61000);

        collector.on("collect", async collected => {
            cmsg.edit({embed: new RichEmbed().setColor("GREEN").addField(`:white_check_mark: Success`, `Closing the ticket..`)})
            setTimeout(function() { msg.channel.delete(); }, 2000);
        });
    }
};