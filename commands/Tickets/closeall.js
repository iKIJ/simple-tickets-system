/**
 * 
 * Simple Tickets System - Made by Lyex
 * Lyex (c) 2019
 * 
 */ 

module.exports = {
    config: {
        name: "closeall", // أسم الأمر الرئيسي..
        aliases: [], // بإمكانك إضافة أختصارات اخرى..
        permission: 4, // مستوى البرمشن 4, فقط لمن يمتلكون صلاحيات أدمن
        category: "tickets"
    },
    exec: async (client, msg, args, storage) => {
        const { RichEmbed } = require("discord.js");

        let cmsg = await msg.channel.send({embed: new RichEmbed().setColor("ORANGE").setTitle(`:warning: Warning`).setDescription(`This action cannot be undo. Please click on :white_check_mark: to confirm that you want to close all the tickets!\nOtherwise just ignore it!`)});
        await cmsg.react("✅");
        let filter = (reaction, user) => reaction.emoji.name === "✅" && user.id === msg.author.id;
        
        let collector = cmsg.createReactionCollector(filter, { max: 1, time: 60000 });

        collector.on("collect", async collected => {
            cmsg.clearReactions();
            cmsg.edit({embed: new RichEmbed().setColor("GREEN").addField(`:white_check_mark: Success`, `Closing all the opened tickets now..`)});
            
            let category = msg.guild.channels.get(storage.tickets.category);
            if(category) category.children.forEach(async channel => { await channel.delete(); });
            else msg.guild.channels.filter(c => c.name.toLowerCase().startsWith("ticket")).forEach(async channel => { await channel.delete(); });

            msg.channel.send({embed: new RichEmbed().setColor("GREEN").setFooter(msg.author.tag, client.user.displayAvatarURL).setDescription(`✅ Successfully closed all opened tickets!`).setTimestamp()});
        });
    }
};