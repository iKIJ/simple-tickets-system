/**
 * 
 * Simple Tickets System - Made by Lyex
 * Lyex (c) 2019
 * 
 */ 

module.exports = {
    config: {
        name: "add", // أسم الأمر الرئيسي..
        aliases: [], // بإمكانك إضافة أختصارات اخرى..
        permission: 0, // مستوى البرمشنات لأستخدام الأمر, 0 = جميع الأعضاء..
        category: "tickets"
    },
    exec: async (client, msg) => {
        const { RichEmbed } = require("discord.js");

        if(!msg.channel.name.toLowerCase().startsWith("ticket-")) {
            let embed = new RichEmbed().setColor("RED").addField(`:x: Forbidden`, `Command has been cancelled - you are not allowed to use this command outside a ticket!`);
            return msg.channel.send({embed: embed});
        }

        let member = msg.mentions.members.first();
        if(!member) {
            let embed = new RichEmbed().setColor("RED").addField(`:x: Invalid Arguments`, `**Usage:** \`add @member\`\n**Example:** \`add @${msg.author.tag}\``);
            return msg.channel.send({embed: embed});
        }
        if(msg.channel.permissionsFor(member).has(['SEND_MESSAGES', 'VIEW_CHANNEL', 'READ_MESSAGE_HISTORY'])) {
            let embed = new RichEmbed().setColor("ORANGE").addField(`:warning: Warning`, `**${member.user.tag}** is already in this ticket!`);
            return msg.channel.send({embed: embed});
        }
        msg.channel.overwritePermissions(member.id, { SEND_MESSAGES: true, VIEW_CHANNEL: true, READ_MESSAGE_HISTORY: true });
        let embed = new RichEmbed().setColor("BLUE").addField(`:white_check_mark: User Added!`, `Successfully added **${member.user.tag}** to the ticket!`).setFooter(`Added by: ${msg.author.tag}`);
        msg.channel.send({embed: embed});
    }
};