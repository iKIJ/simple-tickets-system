/**
 * 
 * Simple Tickets System - Made by Lyex
 * Lyex (c) 2019
 * 
 */ 

const storage = require("../storage");
module.exports = {
    getPerms: (msg) => {
        let permission = 0;
        let [support, supervisor] = [msg.guild.roles.find(role => role.id === storage.tickets.roles.support), msg.guild.roles.find(role => role.id === storage.tickets.roles.supervisor)];
        if(storage.bot.owner.includes(msg.author.id) || msg.guild.owner.user.id === msg.author.id) permission = 5;
        else if(msg.member.hasPermission("ADMINISTRATOR")) permission = 4;
        else if(supervisor && msg.member.roles.some(role => role.id === supervisor.id)) permission = 3;
        else if(support && msg.member.roles.some(role => role.id === support.id)) permission = 2;
        return permission;
    },
    getCmds: (client, category) => {
        let cmds = client.commands.filter(c => c.config.category.toLowerCase() === category.toLowerCase()).map(c => c.config.name).join(", ");
        return cmds;
    }
}